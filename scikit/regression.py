# -*- coding: utf-8 -*-

from sklearn import datasets, svm
from matplotlib import pyplot

digits = datasets.load_digits()


# Features - Xs (1797 examples (rows) x 64[8x8] pixels (features))
print("Xs: "); print(digits.data)
# Target/Label - Y (1 x 1797)
print("Y: "); print(digits.target)

# Set Estimator for train + predict (we need regression [X.yyy] - e.g. RVC, viz ml_map.png)
# Gamma: defines how far the influence of a single training example reaches
# C: corrects classification of training examples against maximization of the decision
clf = svm.SVR(gamma=0.001, C=100.)

# Train! (without Z last items)
z=3
clf.fit(digits.data[:-z], digits.target[:-z])


# Show last z items as graph...
for i in range(len(digits.data) - z , len(digits.data)):
    pyplot.imshow(digits.images[i], cmap='gray')
    pyplot.show()

# ... and predict them!
print("Predicted:", clf.predict(digits.data[-z:]))