# -*- coding: utf-8 -*-

from sklearn import datasets, svm
from matplotlib import pyplot

digits = datasets.load_digits()


# Features - Xs (1797 examples (rows) x 64[8x8] pixels (features))
print("Xs: "); print(digits.data)
# Target/Label - Y (1 x 1797)
print("Y: "); print(digits.target)

# Set Estimator for train + predict (we need classification [0-9] - e.g. linear SVC, viz ml_map.png)
# Gamma: defines how far the influence of a single training example reaches
# C: corrects classification of training examples against maximization of the decision
clf = svm.SVC(gamma=0.001, C=100.)

# Train! (without last item)
clf.fit(digits.data[:-1], digits.target[:-1])


# Show last item as graph...
pyplot.imshow(digits.images[-1:][0], cmap='gray')
pyplot.show()

# ... and predict!
print("Predicted:", clf.predict(digits.data[-1:]))