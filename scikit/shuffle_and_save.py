# -*- coding: utf-8 -*-
from sklearn import datasets, svm
from sklearn.utils import shuffle
import pickle # or joblib - this can work with disc (export/import)

# Common class for incoming data: 2D Features - Xs, 1D Target/Label - Y
class Data:
    def __init__(self, features, target, toShuffle):
        if toShuffle:
            Xs, Y = shuffle(_t.data, _t.target)
        else:
            Xs, Y = features, target
        
        self.features = Xs
        self.target = Y

# Load iris flowers: 3 targets (0 - Setosa, 1 - Versicolour, 2 - Virginica)
_t = datasets.load_iris()

# Create universal object
data = Data(_t.data, _t.target, True)

# Set Estimator for train + predict (we need classification - e.g. linear SVC, viz ml_map.png)
# Gamma: defines how far the influence of a single training example reaches
# C: corrects classification of training examples against maximization of the decision
# kernel: decision function that separates feature space - linear, poly, rbf
clf = svm.SVC(gamma="scale")

# Train! (without last 5 item)
clf.fit(data.features[:-5], data.target[:-5])

# Save trained, clear variable
dumped = pickle.dumps(clf)
clf = None

# Predict from saved vs Real
print("Predicted:", pickle.loads(dumped).predict(data.features[-5:]))
print("Real:", data.target[-5:])