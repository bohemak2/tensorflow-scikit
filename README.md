# Machine Learning
(Google tutorial)[https://developers.google.com/machine-learning/]

## Glossary
### learning rate
A scalar used to train a model via gradient descent. During each iteration, the gradient descent algorithm multiplies the learning rate by the gradient. The resulting product is called the gradient step.

### step
A forward and backward evaluation of one batch.

### batch size
The number of examples in a batch. For example, the batch size of SGD is 1, while the batch size of a mini-batch is usually between 10 and 1000. Batch size is usually fixed during training and inference; however, TensorFlow does permit dynamic batch sizes.

### input function
In TensorFlow, a function that returns input data to the training, evaluation, or prediction method of an Estimator. For example, the training input function returns a batch of features and labels from the training set.

### activation function
A function that takes in the weighted sum of all of the inputs from the previous layer and then generates and passes an output value (typically nonlinear) to the next layer. Used as fn(b + w * x), where fn:
* ReLU - If input is negative or zero, output is 0. If input is positive, output is equal to input. fn: max(0, x)
* Sigmoid - Returning a value between 0 and 1, fn: 1 / (1 + e(-z)), z: regression (b + x1 * w1 + ...)
* Tanh or other functions
* Linear - return as it is

### target
Synonym for label. In supervised learning, the "answer" or "result" portion of an example. Each example in a labeled dataset consists of one or more features and a label. For instance, in a housing dataset, the features might include the number of bedrooms, the number of bathrooms, and the age of the house, while the label might be the house's price. In a spam detection dataset, the features might include the subject line, the sender, and the email message itself, while the label would probably be either "spam" or "not spam."

### loss
Loss function based on the absolute value of the difference between the values that a model is predicting (y') and the actual values (y) of the labels. L1 loss is less sensitive to outliers than L2 loss.

### 'classification/decision' treshold
Limit value for accepting True (1). Classification threshold should always be 0.5, but thresholds are problem-dependent, and are therefore values that you must tune. On the left negatives (0), on the right positives (1).

### bias
Fixed(start) value of regression = <b>b</b>.

### machine learning
A program or system that builds (trains) a predictive model from input data. The system uses the learned model to make useful predictions from new (never-before-seen) data drawn from the same distribution as the one used to train the model. Model could be <b>classification</b> or <b>regression</b>. 

### neural network
Non-linear solving machine, contains layers with neurons. Each layer use activation function. Neural networks aren't necessarily always better than feature crosses, but neural networks do offer a flexible alternative that works well in many cases. 

## Guide
Is There a Standard Heuristic for Model Tuning?
This is a commonly asked question. The short answer is that the effects of different hyperparameters are data dependent. So there are no hard-and-fast rules; you'll need to test on your data.

That said, here are a few rules of thumb that may help guide you:

1. Training error should steadily decrease, steeply at first, and should eventually plateau as training converges.
2. If the training has not converged, try running it for longer.
3. If the training error decreases too slowly, increasing the learning rate may help it decrease faster.
   But sometimes the exact opposite may happen if the learning rate is too high.
4. If the training error varies wildly, try decreasing the learning rate.
   Lower learning rate plus larger number of steps or larger batch size is often a good combination.
5. Very small batch sizes can also cause instability. First try larger values like 100 or 1000, and decrease until you see degradation.

Again, never go strictly by these rules of thumb, because the effects are data dependent. Always experiment and verify.

## Feature Engineering
* Avoid rarely used discrete feature values (group by common)
* Don't mix "magic" values with actual data (use "is_defined" instead -1)
* Prefer clear and obvious meanings (age instead unix timestamp)
* Account for upstream instability (no ids)
* Omitt unimportant parameters (weak correlation on target)

### Other technics
Scaling feature values - converting floating-point feature values from their natural range (for example, 100 to 900) into a standard range (for example, 0 to 1 or -1 to +1).
Handling extreme outliers - one way would be to take the log of every value, "cap" or "clip" the maximum value to an arbitrary value.
Binning values - divide space into "bins/buckets"

### Synthetic feature
Implicate "multi"linearity - mixing more features together by cross (product). Clever way to learn non-linear relations using a linear model.

## Classification (model)
Model for distinguishing <u>discrete</u> classes.<br> 
 A) <b>One class</b> - single T/F, e.g. [ cat = 0 ]<br>
 B) <b>Multi class</b> - multiple T/F, e.g. [ cat = 0, dog = 1, bird = 0].

##### SoftMax
Used in multi class. Total sum of probabilities is equal tp 1. E.g. [ cat 0.1, dog = 0.9, birds = 0 ].
* full - for every possible class.
* candidate - for all the positive labels but only for a random sample of negative labels.

<u>Confusion matrix</u> - shows which classes were misclassified as other classes.

## Regression (model)
Model that outputs <u>continuous</u> (typically, floating-point) values<br>
 A) <b>Linear</b>: <i>y' = b + x1 * w1 + ... + xn * wn</i> <br>
 B) <b>Logistic</b>: same, use Sigmoid <-1; 1> 
 
 ##### Loss
 A) SUMA(i) = (yi'-yi)^2
 
 ##### Regularization
 L1 - encourages small weights to be 0 = penalizes |weight|<br>
 L2 - encourages weights to be small = penalizes weight2.
 
### Steps
Split data on three parts, each for specific step:
 1) Training
 2) Validating 
 3) Testing
 
 Train and validate data together, then compare results with testing data.
 
 #####  Problems
 <b>Overfitting</b> - model that matches the training data so closely that the model fails to make correct predictions on new data.<br>
 <b>Underfitting</b> - model with poor predictive ability because the model hasn't captured the complexity of the training data.
 
### Evaluation
Using treshold for True / False.
Result of predicted vs. real value in 2x2 metrix. True Positive(1) (TP) + True Negative(0) (TN) = predicts of True, False Positive(1) (FP), False Negative(0) (FN)- predicts of False.

* <u>Accuracy</u> - How many times we correctly predict?<br> Scale of all True (correct) predicts (TP + TN) vs. total predicts (TP + TN + FP + FN)
* <u>Precision</u> - How many times is False Positive incorrect? <small>E.g. Emails flagged as spam that were correctly classified</small><br> Scale of True Positive predicts (TP) vs. all Positive predicts (TP + FP)
* <u>Recall</u> - How many times is True Positive missed? <small>E.g. Actual spam emails that were correctly classified</small><br>How much  po Scale of True Positive (TP) predicts vs. True Positive and False Negative (TP + FN)

Left: T/F Negatives, Right: T/F Positives = < TN TN TN TN FN TN FN |treshold| FN TP FP TP TP TP >
<br>
Between Precision (TP / (TP + FP)) and Recall (TP / (TP + FN)) is tension 
<br>Raise (->) treshold = Precision increase (FP down), Recall decrease (FN up).

<b>ROC</b> - performance of a classification model at all classification thresholds. TPR (y) vs FPR (x).
<b>AUC</b> - relative probability that a random positive example is positioned to the right of a random negative example. <0; 1><br>
<b>Prediction bias</b> (not a 'normal' bias) = "average of predictions" - "average of observations(labels)"

## Neural Network
It is a model with components: 
* set of nodes, analogous to neurons, organized in layers
* set of weights representing the connections between each neural network layer and the layer beneath it
* set of biases, one for each node
* activation function that transforms the output of each node in a layer. Different layers may have different activation functions.

Overfitting is a real potential hazard for NNs. You can look at the gap between loss on training data and loss on validation data to help judge if your model is starting to overfit. If the gap starts to grow, that is usually a sure sign of overfitting.

### Improving
<u>Normalize features</u> - Normalize the inputs to the scale <-1, 1>.<br>
<u>Clip features</u> - clip to min/max values.
 
