# Ignore diff CPU - AVX2
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# Import for learning + prediction (Framework: TensorFlow)
import tensorflow as tf
import pandas as pd
import numpy as np
from tensorflow.python.data import Dataset

# Import for MSE
from sklearn import metrics
import math

# Import for plot
from matplotlib import pyplot as plt
from matplotlib import cm

# Here is hypothese: Depends the # of cars on population and area?
states = pd.Series([ 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'District of Columbia', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming' ])
populations = pd.Series([ 741894, 6931071, 2988248, 39250017, 5540545, 3576452, 952065, 681170, 20612439, 10310371, 1428557, 1683140, 12801539, 6633053, 3134693, 2907289, 4436974, 4681666, 1331479, 6016447, 6811779, 9928300, 5519952, 2988726, 6093000, 1042520, 1907116, 2940058, 1334795, 8944469, 2081015, 19745289, 10146788, 757952, 11614373, 3923561, 4093465, 12784227, 1056426, 4961119, 865454, 6651194, 27862596, 3051217, 624594, 8411808, 7288000, 1831102, 5778708, 585501])
areas = pd.Series([ 665384.04, 113990.3, 53178.55, 163696.32, 104093.67, 5543.41, 2488.72, 68.34, 65757.7, 59425.15, 10931.72, 83568.95, 57913.55, 36419.55, 56272.8, 82278.36, 40407.8, 52378.13, 35379.74, 12405.93, 10554.39, 96713.51, 86935.83, 48431.78, 69706.99, 147039.71, 77347.81, 110571.82, 9349.16, 8722.58, 121590.3, 54554.98, 53819.16, 70698.32, 44825.58, 69898.87, 98378.54, 46054.35, 1544.89, 32020.49, 77115.68, 42144.25, 268596.46, 84896.88, 9616.36, 42774.93, 71297.95, 24230.04, 65496.38, 97813.01])
cars = pd.Series([ 712218.24, 5704271.433, 2782058.888, 32970014.28, 5091760.855, 3075748.72, 904461.75, 565371.1, 16407501.44, 8547297.559, 1245701.704, 1888483.08, 10561269.68, 6062610.442, 3291427.65, 2413049.87, 3727058.16, 4260316.06, 1105127.57, 4151348.43, 5585658.78, 8637621, 4802358.24, 2068198.392, 5057190, 1167622.4, 1994843.336, 2360866.574, 1107879.85, 5948071.885, 1825050.155, 10642710.77, 8015962.52, 818588.16, 10569079.43, 3374262.46, 3667744.64, 10201813.15, 875777.154, 4216951.15, 822181.3, 5587002.96, 22206489.01, 2654558.79, 568380.54, 7065918.72, 6340560, 1604045.352, 4969688.88, 667471.14])

# All data + column for label plot
dataFrame = pd.DataFrame({ 'States': states, 'Populations': populations, 'Areas': areas, 'Cars': cars})
labelColumn = "States"

# Now necessary: There is no order of data (States from a-z), but they can be -> need to mix together before split
dataFrame = dataFrame.reindex(np.random.permutation(dataFrame.index))


# Print correlation (Pearson - <-1 (strong indirect); 0 (no corr.) ; 1(strong direct)>) -> then handle (take parameter out) no correleation (|x| < .1) features (now manually, could it be programmable)
print("Correlation:")
print(dataFrame.corr())

# Independent - features (x1...xn) = what model describes (+ added features_input for input fn)
featureColumns = ["Populations", "Areas"]

features = dataFrame[featureColumns].copy()
features_input = dataFrame[featureColumns]
# Dependent - label/target (y) = what we want predict
targetColumn = "Cars"
targetColumnIndex = 2

target = dataFrame[targetColumn]


# Split on train+validate+test data. Train examples (X1..Xn) + targets (Y) repeatedly together, then Validate examples (X1..Xn) + targets (Y). The last .1 is test.
split_rate = 0.8
validate_rate = 0.075

trainings = dataFrame.head(int(states.size * split_rate))
training_labels = trainings[labelColumn]
training_features = trainings[featureColumns]
training_targets = trainings[[targetColumn]]

validations = dataFrame.shift(-int(states.size * split_rate)).dropna().head(int(states.size * (1 - split_rate - validate_rate)))
validation_labels = validations[labelColumn]
validation_features = validations[featureColumns]
validation_targets = validations[[targetColumn]]

tests = dataFrame.tail(int(states.size * validate_rate))
test_labels = tests[labelColumn]
test_features = tests[featureColumns]
test_targets = tests[[targetColumn]]


plt.figure(figsize=(13, 8))

# 1 - Training
training_rmse = [] # for comparance train with prediction

ax = plt.subplot(2, 3 , 1)
ax.set_title("Training Data")

plt.ylabel('Areas')
plt.xlabel('Population')
plt.scatter(training_features["Populations"], training_features["Areas"], cmap="coolwarm", c=training_targets[targetColumn] / training_features["Populations"])
plt.plot()
# Add state label...
for i, txt in enumerate(training_labels.tolist()):
    plt.annotate(txt, (training_features["Populations"].tolist()[i], training_features["Areas"].tolist()[i]))
# ... and remove Label (doesn't work training)
training_features = trainings[featureColumns + [targetColumn]]

# 2 - Validation
ax = plt.subplot(2, 3, 2)
ax.set_title("Validation Data")

plt.ylabel('Areas')
plt.xlabel('Population')

plt.scatter(validation_features["Populations"], validation_features["Areas"], cmap="coolwarm", c=validation_targets[targetColumn] / validation_features["Populations"])
# Add state label...
for i, txt in enumerate(validation_labels):
    plt.annotate(txt, (validation_features["Populations"].tolist()[i], validation_features["Areas"].tolist()[i]))
plt.plot()
# ... and remove Label (doesn't work training)
validation_features = validations[featureColumns + [targetColumn]]

# 3 - Testing
ax = plt.subplot(2, 3, 3)
ax.set_title("Testing Data")

plt.ylabel('Areas')
plt.xlabel('Population')

plt.scatter(test_features["Populations"], test_features["Areas"], cmap="coolwarm", c=test_targets[targetColumn] / test_features["Populations"])
# Add state label...
for i, txt in enumerate(test_labels):
    plt.annotate(txt, (test_features["Populations"].tolist()[i], test_features["Areas"].tolist()[i]))
plt.plot()


# Linear Regression
def input_fn(features, targets, batch_size=1, shuffle=True, num_epochs=None):
    """Trains a linear regression model of one feature.

    Args:
      features: pandas DataFrame of features
      targets: pandas DataFrame of targets
      batch_size: Size of batches to be passed to the model
      shuffle: True or False. Whether to shuffle the data.
      num_epochs: Number of epochs for which data should be repeated. None = repeat indefinitely
    Returns:
      Tuple of (features, labels) for next data batch
    """

    # Convert pandas data into a dict of np arrays.
    features = {key: np.array(value) for key, value in dict(features).items()}

    # Construct a dataset, and configure batching/repeating.
    ds = Dataset.from_tensor_slices((features, targets))  # warning: 2GB limit
    ds = ds.batch(batch_size).repeat(num_epochs)

    # Shuffle the data, if specified.
    if shuffle:
        ds = ds.shuffle(buffer_size=10000)

    # Return the next batch of data.
    features, labels = ds.make_one_shot_iterator().get_next()
    return features, labels

def construct_feature_columns(input_features):
  """Construct the TensorFlow Feature Columns.

  Args:
    input_features: The names of the numerical input features to use.
  Returns:
    A set of feature columns
  """
  return set([tf.feature_column.numeric_column(my_feature)
              for my_feature in input_features])


def train_model(
        learning_rate,
        steps,
        batch_size,
        training_examples,
        training_targets,
        validation_examples,
        validation_targets):
    """Trains a linear regression model of multiple features.

    In addition to training, this function also prints training progress information,
    as well as a plot of the training and validation loss over time.

    Args:
      learning_rate: A `float`, the learning rate.
      steps: A non-zero `int`, the total number of training steps. A training step
        consists of a forward and backward pass using a single batch.
      batch_size: A non-zero `int`, the batch size.
      training_examples: A `DataFrame` containing one or more columns from
        `california_housing_dataframe` to use as input features for training.
      training_targets: A `DataFrame` containing exactly one column from
        `california_housing_dataframe` to use as target for training.
      validation_examples: A `DataFrame` containing one or more columns from
        `california_housing_dataframe` to use as input features for validation.
      validation_targets: A `DataFrame` containing exactly one column from
        `california_housing_dataframe` to use as target for validation.

    Returns:
      A `LinearRegressor` object trained on the training data.
    """

    periods = 10
    steps_per_period = steps / periods

    # Create a linear regressor object.
    my_optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
    my_optimizer = tf.contrib.estimator.clip_gradients_by_norm(my_optimizer, 5.0)
    linear_regressor = tf.estimator.LinearRegressor(
        feature_columns=construct_feature_columns(training_examples),
        optimizer=my_optimizer
    )

    # Create input functions.
    training_input_fn = lambda: input_fn(
        training_examples,
        training_targets[targetColumn],
        batch_size=batch_size)

    predict_training_input_fn = lambda: input_fn(
        training_examples,
        training_targets[targetColumn],
        num_epochs=1,
        shuffle=False)

    predict_validation_input_fn = lambda: input_fn(
        validation_examples,
        validation_targets[targetColumn],
        num_epochs=1,
        shuffle=False)

    # Train the model, but do so inside a loop so that we can periodically assess
    # loss metrics.
    print("Training model...")
    print("RMSE (on training data):")
    validation_rmse = []
    colors = [cm.coolwarm(x) for x in np.linspace(-1, 1, periods)]

    for period in range(0, periods):
        # Train the model, starting from the prior state.
        linear_regressor.train(
            input_fn=training_input_fn,
            steps=steps_per_period,
        )
        # Take a break and compute predictions.
        training_predictions = linear_regressor.predict(input_fn=predict_training_input_fn)
        training_predictions = np.array([item['predictions'][0] for item in training_predictions])

        validation_predictions = linear_regressor.predict(input_fn=predict_validation_input_fn)
        validation_predictions = np.array([item['predictions'][0] for item in validation_predictions])

        # Compute training and validation loss.
        training_root_mean_squared_error = math.sqrt(
            metrics.mean_squared_error(training_predictions, training_targets[targetColumn]))
        validation_root_mean_squared_error = math.sqrt(
            metrics.mean_squared_error(validation_predictions, validation_targets[targetColumn]))
        # Occasionally print the current loss.
        print("period %02d : %0.2f" % (period, training_root_mean_squared_error))
        # Add the loss metrics from this period to our list.
        training_rmse.append(training_root_mean_squared_error)
        validation_rmse.append(validation_root_mean_squared_error)


        ## TODO: Target column? Cheat from linear_regression_periods
        y_extents = np.array([0, dataFrame[targetColumn].max()])

        weight = linear_regressor.get_variable_value('linear/linear_model/%s/weights' % targetColumn)[0]
        bias = linear_regressor.get_variable_value('linear/linear_model/bias_weights')

        x_extents = (y_extents - bias) / weight
        x_extents = np.maximum(np.minimum(x_extents, dataFrame[targetColumn].max()), dataFrame[targetColumn].min())
        y_extents = weight * x_extents + bias
        plt.plot(x_extents, y_extents, color=colors[period])

    print("Model training finished.")

    # Output a graph of loss metrics over periods.
    plt.subplot(2, 3, 4)
    plt.ylabel("RMSE")
    plt.xlabel("Periods")
    plt.plot(training_rmse)
    print("Train RMSE: %0.2f" % training_rmse[len(training_rmse) - 1])

    plt.subplot(2, 3, 5)
    plt.ylabel("RMSE")
    plt.xlabel("Periods")
    plt.plot(validation_rmse)
    print("Validation RMSE: %0.2f" % validation_rmse[len(validation_rmse) - 1])

    return linear_regressor


# Call!
linear_regressor = train_model(
    learning_rate=0.001,
    steps=1000,
    batch_size=10,
    training_examples=training_features,
    training_targets=training_targets,
    validation_examples=validation_features,
    validation_targets=validation_targets)

# Predict on test dataset:
predict_test_input_fn = lambda: input_fn(
      test_features.join(test_targets),
      test_targets,
      num_epochs=1,
      shuffle=False)

test_predictions = linear_regressor.predict(input_fn=predict_test_input_fn)
test_predictions = np.array([item['predictions'][0] for item in test_predictions])

root_mean_squared_error = math.sqrt(
    metrics.mean_squared_error(test_predictions, test_targets))


plt.subplot(2, 3, 6)
plt.ylabel("RMSE")
plt.xlabel("Periods")
training_rmse.append(root_mean_squared_error)
plt.plot(training_rmse)

print("Test RMSE: %0.2f" % root_mean_squared_error)

# Show all plots
plt.show()