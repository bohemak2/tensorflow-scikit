# Ignore diff CPU - AVX2
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# Import for learning + prediction (Framework: TensorFlow)
import tensorflow as tf
import pandas as pd
import numpy as np
from tensorflow.python.data import Dataset

# Import for MSE
from sklearn import metrics
import math

# Import for plot
import seaborn as sns
from matplotlib import pyplot as plt

# 1 - Here is hypothese: Recommend Tag by previous task relation during typing task name
# Target/Label (0(!important)...n)
tags = pd.Series([ 'Art', 'News', 'Sport', 'Technology', 'Travel' ])
task_tag = pd.Series([ 2, 4, 1, 1, 5, 2, 1, 4, 4, 3, 3, 5, 5, 2, 2, 1, 3, 4, 2, 1, 2, 5, 5, 2, 1, 4, 2, 5, 4, 3, 2, 3, 3, 4, 3, 5, 2, 4, 5, 5, 5, 3, 3, 4, 5, 2, 2, 3, 2, 1, 4, 1, 4, 4, 4, 4, 1, 3, 5, 3, 3, 5, 5, 3, 4, 2, 1, 5, 3, 3, 1, 5, 2, 5, 2, 4, 2, 1, 2, 3, 3, 2, 3, 2, 4, 4, 1, 3, 2, 3, 2, 1, 4, 3, 5, 1, 2, 5, 3, 5 ]) - 1
# Features
task_owner = pd.Series([ 7, 6, 9, 4, 8, 8, 1, 5, 8, 2, 9, 7, 5, 9, 7, 3, 6, 10, 2, 3, 1, 1, 1, 8, 8, 7, 2, 7, 5, 6, 7, 3, 4, 5, 3, 5, 7, 1, 8, 2, 8, 2, 1, 3, 1, 4, 9, 4, 4, 3, 5, 1, 2, 3, 10, 2, 1, 6, 6, 8, 3, 1, 3, 9, 6, 7, 1, 2, 2, 1, 9, 3, 9, 4, 9, 4, 5, 2, 6, 6, 7, 5, 7, 1, 7, 5, 6, 3, 9, 3, 7, 10, 8, 1, 8, 10, 5, 10, 1, 4 ])
task_assigner = pd.Series([ 1, 6, 4, 2, 1, 6, 4, 3, 6, 8, 3, 7, 3, 3, 6, 8, 2, 4, 8, 3, 2, 2, 6, 8, 2, 5, 1, 8, 2, 7, 7, 7, 3, 8, 2, 7, 7, 3, 2, 4, 3, 7, 8, 8, 6, 7, 3, 6, 7, 1, 1, 7, 5, 2, 6, 3, 3, 1, 4, 3, 4, 4, 4, 1, 4, 3, 2, 2, 3, 2, 1, 1, 2, 6, 1, 4, 2, 6, 8, 7, 7, 3, 8, 2, 8, 4, 8, 6, 4, 3, 8, 3, 7, 1, 6, 3, 2, 1, 6, 7 ])
#task_name = pd.Series(tf.string_to_number(str., tf.int32) for str in [ "Make Paintings",  "Luggage Went",  "Event Accident",  "Da Vinci Paintings",  "Da Vinci Prize", "Make Won",  "Car Train",  "Starts Event",  "Won Prize",  "game Football",  "Running Running",  "Basketball Basketball",  "Nano Math",  "Da Vinci Paintings",  "game play",  "game Football",  "Athletics Hockey",  "Accident Accident",  "Make Won",  "Gossip Terrible",  "Consist Computer",  "Starts Terrible",  "Make Works",  "Review Autonomous",  "Prize Piece",  "Went Luggage",  "Accident Event",  "play Football",  "Science Review",  "Accident Accident",  "Starts Terrible",  "Car Car",  "Won Piece",  "Basketball game",  "Review Autonomous",  "Autonomous Computer",  "Computer Autonomous",  "Prize Paintings",  "Football play",  "Event Starts",  "Works Prize",  "Athletics Basketball",  "Go Went",  "Terrible Starts",  "Works Prize",  "Review Nano",  "Holiday Went",  "Go Luggage",  "Nano Review",  "Athletics Basketball",  "Starts Starts",  "Science Consist",  "Works Prize",  "Computer Math",  "Car Luggage",  "Review Nano",  "Math Autonomous",  "Science Nano",  "Paintings Piece",  "Car Car",  "Piece Piece",  "Consist Computer",  "Event Starts",  "Hockey play",  "Paintings Piece",  "Make Works",  "Math Review",  "Won Paintings",  "Science Nano",  "Da Vinci Piece",  "Terrible Event",  "Happens Gossip",  "Holiday Car",  "Go Travel",  "Make Piece",  "play Hockey",  "Go Travel",  "Math Computer",  "Computer Math",  "Luggage Holiday",  "Works Paintings",  "Won Piece",  "Gossip Starts",  "Terrible Gossip",  "Da Vinci Make",  "Science Nano",  "Plane Holiday",  "Da Vinci Make",  "game play",  "Luggage Car",  "Da Vinci Prize",  "Paintings Piece",  "Event Terrible",  "Da Vinci Paintings",  "Happens Happens",  "Review Computer",  "Piece Won",  "Athletics Basketball",  "Nano Nano",  "Prize Paintings" ])

# Size of variance + models
task_size = task_tag.size
n_class_size = tags.size

# All data + column for label plot
dataFrame = pd.DataFrame({ 'Tags': task_tag, 'Owners': task_owner, 'Assigners': task_assigner}) # 'Names': task_name})

# Now necessary: There is no order of data (States from a-z), but they can be -> need to mix together before split
dataFrame = dataFrame.reindex(np.random.permutation(dataFrame.index))


# Print correlation (Pearson - <-1 (strong indirect); 0 (no corr.) ; 1(strong direct)>) -> then handle (take parameter out) no correleation (|x| < .1) features (now manually, could it be programmable)
print("Correlation:")
print(dataFrame.corr())

# Dependent - label/target (y) = what we want predict
targetColumn = "Tags"

target = dataFrame[[targetColumn]]

# Independent - all without target
featureColumns = dataFrame.keys().to_list()
featureColumns.remove(targetColumn)

features = dataFrame[featureColumns].copy()
features_input = dataFrame[featureColumns]


# 2 - Split on train+validate+test data. Train examples (X1..Xn) + targets (Y) repeatedly together, then Validate examples (X1..Xn) + targets (Y). The last .1 is test.
split_rate = 0.8
validate_rate = 0.075

trainings = dataFrame.head(int(task_size * split_rate))
training_features = trainings[featureColumns]
training_targets = trainings[[targetColumn]]

validations = dataFrame.shift(-int(task_size * split_rate)).dropna().head(int(task_size * (1 - split_rate - validate_rate)))
validation_features = validations[featureColumns]
validation_targets = validations[[targetColumn]]

tests = dataFrame.tail(int(task_size * validate_rate))
test_features = tests[featureColumns]
test_targets = tests[[targetColumn]]


plt.figure(figsize=(13, 8))

# 1 - Training
training_rmse = [] # for comparance train with prediction
# Linear Regression
def input_fn(features, targets, batch_size=1, shuffle=True, num_epochs=None):
    """Trains a linear regression model of one feature.

    Args:
      features: pandas DataFrame of features
      targets: pandas DataFrame of targets
      batch_size: Size of batches to be passed to the model
      shuffle: True or False. Whether to shuffle the data.
      num_epochs: Number of epochs for which data should be repeated. None = repeat indefinitely
    Returns:
      Tuple of (features, labels) for next data batch
    """

    # Convert pandas data into a dict of np arrays.
    features = {key: np.array(value) for key, value in dict(features).items()}

    # Construct a dataset, and configure batching/repeating.
    ds = Dataset.from_tensor_slices((features, targets))  # warning: 2GB limit
    ds = ds.batch(batch_size).repeat(num_epochs)

    # Shuffle the data, if specified.
    if shuffle:
        ds = ds.shuffle(buffer_size=10000)

    # Return the next batch of data.
    features, labels = ds.make_one_shot_iterator().get_next()
    return features, labels

def construct_feature_columns(input_features):
  """Construct the TensorFlow Feature Columns.

  Args:
    input_features: The names of the numerical input features to use.
  Returns:
    A set of feature columns
  """
  return set([tf.feature_column.numeric_column(my_feature)
              for my_feature in input_features])


def train_nn_model(
        learning_rate,
        steps,
        batch_size,
        hidden_units,
        training_examples,
        training_targets,
        validation_examples,
        validation_targets):
    """Trains a linear regression model of multiple features.

    In addition to training, this function also prints training progress information,
    as well as a plot of the training and validation loss over time.

    Args:
      learning_rate: A `float`, the learning rate.
      steps: A non-zero `int`, the total number of training steps. A training step
        consists of a forward and backward pass using a single batch.
      batch_size: A non-zero `int`, the batch size.
      hidden_units: A `list` of int values, specifying the number of neurons in each layer.
      training_examples: A `DataFrame` containing one or more columns from
        `california_housing_dataframe` to use as input features for training.
      training_targets: A `DataFrame` containing exactly one column from
        `california_housing_dataframe` to use as target for training.
      validation_examples: A `DataFrame` containing one or more columns from
        `california_housing_dataframe` to use as input features for validation.
      validation_targets: A `DataFrame` containing exactly one column from
        `california_housing_dataframe` to use as target for validation.

    Returns:
       A `DNNRegressor` object trained on the training data.
    """

    periods = 10
    steps_per_period = steps / periods

    # Create a linear regressor object.
    my_optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
    my_optimizer = tf.contrib.estimator.clip_gradients_by_norm(my_optimizer, 5.0)

    training_errors = []
    validation_errors = []

    # Classifier, not Regressor
    dnn_classifier = tf.estimator.DNNClassifier(
        feature_columns=construct_feature_columns(training_examples),
        hidden_units=hidden_units,
        optimizer=my_optimizer,
        config=tf.contrib.learn.RunConfig(keep_checkpoint_max=1),
        # Class labels are integers representing the class index (i.e. values from 0 to n_classes-1). Arbitrary label values (e.g. string labels), convert to class indices.
        n_classes=n_class_size,
        # label_vocabulary=tags.to_list()
    )

    # Create input functions.
    training_input_fn = lambda: input_fn(
        training_examples,
        training_targets,
        batch_size=batch_size)

    predict_training_input_fn = lambda: input_fn(
        training_examples,
        training_targets,
        num_epochs=1,
        shuffle=False)

    predict_validation_input_fn = lambda: input_fn(
        validation_examples,
        validation_targets,
        num_epochs=1,
        shuffle=False)

    # Train the model, but do so inside a loop so that we can periodically assess
    # loss metrics.
    print("Training model...")
    print("RMSE (on training data):")
    # colors = [cm.coolwarm(x) for x in np.linspace(-1, 1, periods)]

    for period in range(0, periods):
        # Train the model, starting from the prior state.
        dnn_classifier.train(
            input_fn=training_input_fn,
            steps=steps_per_period,
        )

        # Take a break and compute predictions + probabilities.
        training_predictions = list(dnn_classifier.predict(input_fn=predict_training_input_fn))
        training_probabilities = np.array([item['probabilities'] for item in training_predictions])
        training_pred_class_id = np.array([item['class_ids'][0] for item in training_predictions])
        training_pred_one_hot = tf.keras.utils.to_categorical(training_pred_class_id, n_class_size)

        validation_predictions = list(dnn_classifier.predict(input_fn=predict_validation_input_fn))
        validation_probabilities = np.array([item['probabilities'] for item in validation_predictions])
        validation_pred_class_id = np.array([item['class_ids'][0] for item in validation_predictions])
        validation_pred_one_hot = tf.keras.utils.to_categorical(validation_pred_class_id, n_class_size)

        # Compute training and validation errors.
        training_log_loss = metrics.log_loss(training_targets, training_pred_one_hot)
        validation_log_loss = metrics.log_loss(validation_targets, validation_pred_one_hot)
        # Occasionally print the current loss.
        print("  period %02d : %0.2f" % (period, validation_log_loss))
        # Add the loss metrics from this period to our list.
        training_errors.append(training_log_loss)
        validation_errors.append(validation_log_loss)


    print("Model training finished.")

    final_predictions = dnn_classifier.predict(input_fn=predict_validation_input_fn)
    final_predictions = np.array([item['class_ids'][0] for item in final_predictions])

    accuracy = metrics.accuracy_score(validation_targets, final_predictions)
    print("Final accuracy (on validation data): %0.2f" % accuracy)

    # Output a graph of loss metrics over periods.
    plt.subplot(2, 2, 3)
    plt.ylabel("LogLoss")
    plt.xlabel("Periods")
    plt.title("LogLoss vs. Periods")
    plt.plot(training_errors, label="training")
    plt.plot(validation_errors, label="validation")
    plt.legend()

    # Output a plot of the confusion matrix.
    plt.subplot(2, 2, 4)
    cm = metrics.confusion_matrix(validation_targets, final_predictions)
    # Normalize the confusion matrix by row (i.e by the number of samples
    # in each class).
    cm_normalized = cm.astype("float") / cm.sum(axis=1)[:, np.newaxis]
    ax = sns.heatmap(cm_normalized, cmap="bone_r")
    ax.set_aspect(1)
    plt.title("Confusion matrix")
    plt.ylabel("True label")
    plt.xlabel("Predicted label")

    return dnn_classifier


# Call with hidden layers - list of # of neurons (nodes) in each layer [1st, 2nd, ... ]. By default, all hidden layers will use ReLu activation and will be fully connected.
dnn_classifier = train_nn_model(
    learning_rate=0.001,
    steps=100,
    batch_size=10,
    hidden_units=[10, 8],
    training_examples=training_features,
    training_targets=training_targets,
    validation_examples=validation_features,
    validation_targets=validation_targets)

# Prediction in train_nn_model

# Predict on test dataset:
# predict_test_input_fn = lambda: input_fn(
#       test_features.join(test_targets),
#       test_targets,
#       num_epochs=1,
#       shuffle=False)
#
# test_predictions = dnn_regressor.predict(input_fn=predict_test_input_fn)
# test_predictions = np.array([item['probabilities'] for item in test_predictions])
#
# root_mean_squared_error = math.sqrt(
#     metrics.mean_squared_error(test_predictions, test_targets))
#
# plt.subplot(2, 2, 3)
# plt.ylabel("RMSE")
# plt.xlabel("Periods")
# training_rmse.append(root_mean_squared_error)
# plt.plot(training_rmse)
#
# print("Test RMSE: %0.2f" % root_mean_squared_error)
#
# # Show all plots
# plt.show()

# Visualize the weights of the first hidden layer.
print(dnn_classifier.get_variable_names())

weights0 = dnn_classifier.get_variable_value("dnn/hiddenlayer_0/kernel")

print("weights0 shape:", weights0.shape)

plt.subplot(2, 2, 1)
num_nodes = weights0.shape[1]
num_rows = int(math.ceil(num_nodes / n_class_size))
fig, axes = plt.subplots(num_rows, n_class_size, figsize=(20, 2 * num_rows))
for coef, ax in zip(weights0.T, axes.ravel()):
    # Weights in coef is reshaped from 1xN to MxN.
    ax.matshow(coef.reshape(1, 2), cmap=plt.cm.pink)
    ax.set_xticks(())
    ax.set_yticks(())

plt.show()