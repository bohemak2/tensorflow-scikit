# Ignore diff CPU - AVX2
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# Import for learning + prediction (Framework: TensorFlow)
import tensorflow as tf
import pandas as pd
import numpy as np
from tensorflow.python.data import Dataset

# Import for MSE
from sklearn import metrics
import math

# Import for plot
from matplotlib import pyplot as plt

city_names = pd.Series(['San Francisco', 'San Jose', 'Sacramento', 'Seattle', 'Oklahoma' ])
population = pd.Series([852469, 1015785, 485199, 608660, 579999])
cars = pd.Series([20000, 22000, 12000, 19000, 13000])

# All data + column for label plot
dataFrame = pd.DataFrame({ 'City': city_names, 'Population': population, 'Cars': cars })
labelColumn = "City"
# Not necessary: dataFrame = dataFrame.reindex(np.random.permutation(dataFrame.index))

# Independent - features (x1...xn) = what model describes (+ added features_input for input fn)
featureColumn = "Population"

features = dataFrame[featureColumn]
features_input = dataFrame[[featureColumn]]
# Dependent - label/target (y) = what we want predict
targetColumn = "Cars"

target = dataFrame[targetColumn]

# Linear Regression
# Set Optimizer: Model (Mini-batch SGD) + Learning rate (0.1)
optimizer=tf.compat.v1.train.GradientDescentOptimizer(learning_rate=0.001)
# Prevent of large magnitude during regressing
optimizer = tf.contrib.estimator.clip_gradients_by_norm(optimizer, 5.0)

# Set Regressor - it holds data for whole training / predicting:
linear_regressor = tf.estimator.LinearRegressor(
    feature_columns=[tf.feature_column.numeric_column(featureColumn)],
    optimizer=optimizer
)

# Set input/model function for training
def input_fn(features, targets, batch_size=1, shuffle=True, num_epochs=None):
    """Trains a linear regression model of one feature.

    Args:
      features: pandas DataFrame of features
      targets: pandas DataFrame of targets
      batch_size: Size of batches to be passed to the model
      shuffle: True or False. Whether to shuffle the data.
      num_epochs: Number of epochs for which data should be repeated. None = repeat indefinitely
    Returns:
      Tuple of (features, labels) for next data batch
    """

    # Convert pandas data into a dict of np arrays.
    features = {key: np.array(value) for key, value in dict(features).items()}

    # Construct a dataset, and configure batching/repeating.
    ds = Dataset.from_tensor_slices((features, targets))  # warning: 2GB limit
    ds = ds.batch(batch_size).repeat(num_epochs)

    # Shuffle the data, if specified.
    if shuffle:
        ds = ds.shuffle(buffer_size=10000)

    # Return the next batch of data.
    features, labels = ds.make_one_shot_iterator().get_next()
    return features, labels

# And train! The result is in features.
_ = linear_regressor.train(
    input_fn = lambda:input_fn(features_input, target),
    steps=100
)

# ...and predict!
prediction_input_fn =lambda:input_fn(features_input, target, num_epochs=1, shuffle=False)
predictions = linear_regressor.predict(input_fn=prediction_input_fn)
# Format predictions as a NumPy array
predictions = np.array([item['predictions'][0] for item in predictions])

# Print (Regression) Errors - Mean Squared Error (MSE) and Root Mean Squared Error (RMSE).
mean_squared_error = metrics.mean_squared_error(predictions, target)
root_mean_squared_error = math.sqrt(mean_squared_error)
print("Mean Squared Error (on training data): %0.3f" % mean_squared_error)
print("Root Mean Squared Error (on training data): %0.3f" % root_mean_squared_error)


# Plot y' = b + w*x
sample = dataFrame.sample(n=dataFrame[featureColumn].size)
# Get the min and max total_rooms values.
x_0 = sample[featureColumn].min()
x_1 = sample[featureColumn].max()

# Retrieve the final weight and bias (b) generated during training
weight = linear_regressor.get_variable_value('linear/linear_model/' + featureColumn + '/weights')[0]
bias = linear_regressor.get_variable_value('linear/linear_model/bias_weights')

# Get the predicted median_house_values for the min and max total_rooms values.
y_0 = weight * x_0 + bias
y_1 = weight * x_1 + bias

# Plot our regression line from (x_0, y_0) to (x_1, y_1).
plt.plot([x_0, x_1], [y_0, y_1], c='r')

# Label the graph axes.
plt.ylabel(targetColumn)
plt.xlabel(featureColumn)

# Plot a scatter plot from our data sample.
plt.scatter(sample[featureColumn], sample[targetColumn])

# + Annotation by first column
for i, txt in enumerate(dataFrame[labelColumn]):
    plt.annotate(txt, (sample[featureColumn][i], sample[targetColumn][i]))

# Display graph
plt.show()


## Help for setting parameters
# Is There a Standard Heuristic for Model Tuning?
# This is a commonly asked question. The short answer is that the effects of different hyperparameters are data dependent. So there are no hard-and-fast rules; you'll need to test on your data.
#
# That said, here are a few rules of thumb that may help guide you:
#
# 1) Training error should steadily decrease, steeply at first, and should eventually plateau as training converges.
# 2) If the training has not converged, try running it for longer.
# 3) If the training error decreases too slowly, increasing the learning rate may help it decrease faster.
#    But sometimes the exact opposite may happen if the learning rate is too high.
# 4) If the training error varies wildly, try decreasing the learning rate.
#    Lower learning rate plus larger number of steps or larger batch size is often a good combination.
# 5) Very small batch sizes can also cause instability. First try larger values like 100 or 1000, and decrease until you see degradation.
#
# Again, never go strictly by these rules of thumb, because the effects are data dependent. Always experiment and verify.

# Src: https://colab.research.google.com/notebooks/mlcc/first_steps_with_tensor_flow.ipynb